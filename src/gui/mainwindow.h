#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "IMediaLibrary.h"

class QString;
class MediaLibrary;
class QListWidgetItem;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow, IMediaLibraryCb
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void statusChanged( const QString& message );

private slots:
    void on_actionNew_Database_triggered();
    void on_actionAdd_Folder_triggered();
    void on_actionLoad_Database_triggered();

    void updateStatusBar( const QString& message );
    void updateThumbnail( const QImage& thumb );
private:

    void onMediaAdded( std::vector<MediaPtr> ) override {}
    void onMediaUpdated( std::vector<MediaPtr> ) override {}
    void onMediaDeleted( std::vector<int64_t> ) override {}
    void onDiscoveryStarted(const std::string&) override {}
    void onDiscoveryCompleted(const std::string&) override;
    void onReloadStarted( const std::string& ) override {}
    void onReloadCompleted( const std::string& ) override {}
    void onArtistsAdded( std::vector<ArtistPtr> artist ) override;
    void onArtistsModified( std::vector<ArtistPtr> ) override {}
    void onArtistsDeleted( std::vector<int64_t> ) override {}
    void onAlbumsAdded( std::vector<AlbumPtr> ) override {}
    void onAlbumsModified( std::vector<AlbumPtr> ) override {}
    void onAlbumsDeleted( std::vector<int64_t> ) override {}
    void onTracksAdded( std::vector<AlbumTrackPtr> ) override {}
    void onTracksDeleted( std::vector<int64_t> ) override {}
    void onParsingStatsUpdated( uint32_t percent ) override;

    Ui::MainWindow      *ui;
    QString          m_dbPath;
    QString          m_thumbPath;

    MediaLibrary                            *m_ml;
};

#endif // MAINWINDOW_H
