#include "mediatreewidget.h"

#include "IArtist.h"
#include "IAlbum.h"
#include "IMedia.h"

#include <QDebug>


MediaTreeWidget::MediaTreeWidget( QWidget *parent ):
    QTreeWidget( parent )
{
    connect( this, &MediaTreeWidget::itemClicked, this, &MediaTreeWidget::onItemClicked);
}

MediaTreeWidget::~MediaTreeWidget()
{
}

QImage MediaTreeWidget::getThumbnailFromItem(QTreeWidgetItem *wItem)
{
    return QImage( QString::fromStdString(
                             wItem->data( 1, Qt::UserRole ).value<std::shared_ptr<IMedia> >()->thumbnail()
                             )
                   );
}

void MediaTreeWidget::setArtists( std::vector<std::shared_ptr<IArtist> > artists)
{
    clear();
    qDebug() << artists.size();
    for ( std::shared_ptr<IArtist> artist: artists) {
        addArtist( artist );
    }

    updateThumbnails();
}

void MediaTreeWidget::addArtist(std::shared_ptr<IArtist> artist)
{
    qDebug() << QString::fromStdString( artist->name() );
    QTreeWidgetItem *artistItem = new QTreeWidgetItem( this );
    artistItem->setText( 1, QString::fromStdString( artist->name() ) );
    for ( std::shared_ptr<IAlbum> album: artist->albums() ) {
        addAlbum( album, artistItem );
    }
}

void MediaTreeWidget::addAlbum(std::shared_ptr<IAlbum> album)
{
    QTreeWidgetItem *albumItem = new QTreeWidgetItem( this );
    albumItem->setText( 1, QString::fromStdString( album->title() ) );
    addTracks( album->tracks(), albumItem );
}

void MediaTreeWidget::addAlbum(std::shared_ptr<IAlbum> album, QTreeWidgetItem *parent)
{
    QTreeWidgetItem *albumItem = new QTreeWidgetItem;
    albumItem->setText( 1, QString::fromStdString( album->title() ) );
    parent->addChild( albumItem );
    addTracks( album->tracks(), albumItem );
}

void MediaTreeWidget::addTracks(std::vector<std::shared_ptr<IMedia> > medias)
{
    for ( std::shared_ptr<IMedia> media: medias) {
        QTreeWidgetItem *mediaItem = new QTreeWidgetItem( this );
        mediaItem->setText( 1, QString::fromStdString( media->title() ) );
        mediaItem->setData( 1, Qt::UserRole, QVariant::fromValue( media ) );
        m_mediaItems.append( mediaItem );
    }

    updateThumbnails();
}

void MediaTreeWidget::addTracks(std::vector<std::shared_ptr<IMedia> > medias, QTreeWidgetItem *parent)
{
    for ( std::shared_ptr<IMedia> media: medias) {
        QTreeWidgetItem *mediaItem = new QTreeWidgetItem;
        mediaItem->setText( 1, QString::fromStdString( media->title() ) );
        mediaItem->addChild( parent );
        mediaItem->setData( 1, Qt::UserRole, QVariant::fromValue( media ) );
        m_mediaItems.append( mediaItem );
    }

    updateThumbnails();
}

void MediaTreeWidget::updateThumbnails()
{
    for ( QTreeWidgetItem *wItem: m_mediaItems ) {
        QImage thumb = MediaTreeWidget::getThumbnailFromItem( wItem );
        if ( !thumb.isNull() )
            wItem->setIcon( 0, QIcon( QPixmap::fromImage( thumb ) ) );
    }
}

void MediaTreeWidget::onItemClicked(QTreeWidgetItem *wItem, int column)
{
    Q_UNUSED( column )
    QImage thumb = MediaTreeWidget::getThumbnailFromItem( wItem );
    if ( !thumb.isNull() )
        emit thumbnailChanged( thumb );
}
