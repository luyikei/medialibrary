#ifndef MEDIATREEWIDGET_H
#define MEDIATREEWIDGET_H

#include <QTreeWidget>
#include <QList>

#include "IMedia.h"

Q_DECLARE_METATYPE( std::shared_ptr<IMedia> )

class IArtist;

class MediaTreeWidget : public QTreeWidget
{
    Q_OBJECT
public:
    explicit MediaTreeWidget( QWidget *parent = 0 );
    ~MediaTreeWidget();

    static QImage getThumbnailFromItem( QTreeWidgetItem *wItem );

    void setArtists( std::vector<std::shared_ptr<IArtist> > artists);
    void addArtist( std::shared_ptr<IArtist> artist );

    void addAlbum( std::shared_ptr<IAlbum> album );
    void addAlbum( std::shared_ptr<IAlbum> album, QTreeWidgetItem *parent );

    void addTracks( std::vector<std::shared_ptr<IMedia> > medias );
    void addTracks( std::vector<std::shared_ptr<IMedia> > medias, QTreeWidgetItem *parent );

    void updateThumbnails();

signals:
    void thumbnailChanged( const QImage &thumb );

private slots:
    void onItemClicked( QTreeWidgetItem *wItem, int column );

private:
    QList<QTreeWidgetItem*> m_mediaItems;
};

#endif // MEDIATREEWIDGET_H
