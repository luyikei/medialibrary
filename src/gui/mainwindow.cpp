#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>

#include "MediaLibrary.h"
#include "Media.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_ml(new MediaLibrary)
{
    ui->setupUi(this);
    connect( this, &MainWindow::statusChanged, this, &MainWindow::updateStatusBar );
    connect( ui->mediaListWidget, &MediaTreeWidget::thumbnailChanged, this, &MainWindow::updateThumbnail );
    QMessageBox::information(this, tr( "Setup Please!" ), tr( "Please setup your database from the toolbar.") );
}

MainWindow::~MainWindow()
{
    delete ui;
    delete m_ml;
}

void MainWindow::onDiscoveryCompleted(const std::string &)
{
    m_ml->reload();

    ui->mediaListWidget->addTracks( m_ml->audioFiles( medialibrary::SortingCriteria::Default, false ) );
    ui->mediaListWidget->addTracks( m_ml->videoFiles( medialibrary::SortingCriteria::Default, false ) );
}

void MainWindow::onArtistsAdded(std::vector<ArtistPtr> artist)
{
    ui->artistListWidget->setArtists( artist );
}

void MainWindow::onParsingStatsUpdated(uint32_t percent)
{
    ui->mediaListWidget->updateThumbnails();
    if ( percent < 100 ) {
        emit statusChanged( QString("Parsed %1%..." ).arg( QString::number( percent ) ) );
    } else {
        emit statusChanged( "Parsing was completed!" );
    }
}

void MainWindow::on_actionNew_Database_triggered()
{
    m_dbPath = QFileDialog::getSaveFileName( this, tr( "Specify your path for database" ), QString(), tr( "SQLite database (*.db)" ) );
    m_thumbPath = QFileDialog::getExistingDirectory( this, tr( "Specify your folder to save thumbnails" ) );
    if ( m_dbPath.isEmpty() || m_thumbPath.isEmpty() )
        return;
    m_dbPath.replace( ".db", "" ).append( ".db" );

    qDebug() << m_ml->initialize( m_dbPath.toStdString(), m_thumbPath.toStdString(), static_cast<IMediaLibraryCb*>(this) );
}

void MainWindow::on_actionAdd_Folder_triggered()
{
    QString folderPath = QFileDialog::getExistingDirectory( this, tr( "Specify your folder to discover audio tracks" ) );
    m_ml->discover( folderPath.toStdString() );

    ui->mediaListWidget->clear();
    ui->mediaListWidget->addTracks( m_ml->audioFiles( medialibrary::SortingCriteria::Default, false) );
    ui->mediaListWidget->addTracks( m_ml->videoFiles( medialibrary::SortingCriteria::Default, false) );
    ui->artistListWidget->setArtists( m_ml->artists( medialibrary::SortingCriteria::Default, false ) );
}

void MainWindow::on_actionLoad_Database_triggered()
{
    m_dbPath = QFileDialog::getOpenFileName( this, tr( "Specify your path for database" ), QString(), tr( "SQLite database (*.db)" ) );
    if ( m_dbPath.isEmpty() )
        return;

    qDebug() << m_ml->initialize( m_dbPath.toStdString(), m_thumbPath.toStdString(), static_cast<IMediaLibraryCb*>(this) );

    ui->mediaListWidget->clear();
    ui->mediaListWidget->addTracks( m_ml->audioFiles( medialibrary::SortingCriteria::Default, false) );
    ui->mediaListWidget->addTracks( m_ml->videoFiles( medialibrary::SortingCriteria::Default, false) );
    ui->artistListWidget->setArtists( m_ml->artists( medialibrary::SortingCriteria::Default, false ) );
}

void MainWindow::updateStatusBar(const QString &message)
{
    ui->statusBar->showMessage( message, 3000 );
}

void MainWindow::updateThumbnail(const QImage &thumb)
{
    ui->thumbnail->setPixmap( QPixmap::fromImage( thumb ) );
}
